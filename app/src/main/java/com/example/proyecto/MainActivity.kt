package com.example.proyecto

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btn_multiplicar.setOnClickListener{ multiplicar() }
        btn_numeros.setOnClickListener{ Numeros() }
        btn_palabras.setOnClickListener{ Palabras() }
        btn_palindroma.setOnClickListener{ Palindroma()}
    }

     fun multiplicar(){
        if(!txt_primero.text.isNullOrEmpty() && !txt_segundo.text.isNullOrEmpty()){
            var primero:Int = txt_primero.text.toString().toInt()
            var segundo:Int = txt_segundo.text.toString().toInt()
            var resultado:Int = 0
            var x = 0
            var esPositivo:Boolean = primero > 0

            while (x < Math.abs(primero)) {
                resultado = if (esPositivo) resultado + segundo else resultado - segundo
                x++
            }
            lbl_respuesta.text = resultado.toString()

        }
    }

    fun Numeros(){
        if(!txt_numeros.text.isNullOrEmpty()){
            /*var num:MutableList<Int> = mutableListOf()
            var numerostxt:String = txt_numeros.text.toString()
            numerostxt.split(' ')
            var chunks = numerostxt.chunked(1)
            chunks.forEach{
                num.add(it.max().toString().toInt())
            }
            lbl_respuesta2.text = "Los numeros son= ${num.toString()}, El valor minimo es= ${num.min()} El valor maximo es: ${num.max()}"*/

            val numeros = txt_numeros.text.toString().split(",").toTypedArray()
            var numeroGrande: Int = 0
            for (numero in numeros) {
                var numero2:Int? = numero.toIntOrNull()
                if (numero2 != null) {
                    if (numero2 > numeroGrande) {
                        numeroGrande = numero2
                    }
                }

            }
            lbl_respuesta2.text = "El numero mas grande es " + numeroGrande.toString()
        }
    }

    fun Palabras(){
        if(!txt_palabras.text.isNullOrEmpty()) {
            /*var palabras: MutableList<String> = mutableListOf()
            var palabrastxt:String = txt_palabras.text.toString()
            palabrastxt.split(' ')
            var chunks = palabrastxt.chunked(1)
            chunks.forEach {
                palabras.add(it.toString())
            }
            var x = palabras.filter { it == palabras.first() }
            var y = palabras.filter { it == palabras.last() }
            if (x.size > 1) {
                var z = x.size
                lbl_respuesta3.text = "Los palabras son= $x, el numero de repetidos es= $z"
            } else if (y.size > 1) {
                var z = x.size
                lbl_respuesta3.text = "Los palabras son= $y, el numero de repetidos es= $z"
            } else {
                lbl_respuesta3.text =  "No hay palabras repetidas"
            }*/
            val palabras = txt_palabras.text.toString().toLowerCase().split(",").toTypedArray()
            var listaRepetidos:MutableMap<String, Int> = mutableMapOf()
            for (palabra in palabras) {
                if (listaRepetidos.isNotEmpty() && listaRepetidos[palabra] != null) {
                    //listaRepetidos.computeIfPresent(palabra) { _, valor -> valor + 1 } //La mejor forma pero solo funciona en api 24 para arriba
                    listaRepetidos[palabra] = listaRepetidos[palabra]!!.plus(1)
                } else {
                    listaRepetidos[palabra] = 1
                }
            }

            lbl_respuesta3.text = "La palabra que mas se repite es: " + listaRepetidos.entries.reduce{acc, item -> if(acc != null && acc.value > item.value) acc else item}
        }
    }

    fun Palindroma() {
        if (!txt_palindroma.text.isNullOrEmpty()) {
            var palabra: String = txt_palindroma.text.toString()
            var reversa: String = palabra.reversed()

            if (palabra == reversa) {
                lbl_respuesta4.text = "Los palabras es= $palabra, si es Palindroma"
            } else {
                lbl_respuesta4.text = "Los palabras es= $palabra, no es Palindroma"
            }
        }
    }


    /*fun showErrorName(){
        Toast.makeText( context: this, text: "Te falta llenar un campo en multilpliacion", Toast.LENGTH_SHORT).show()
    }*/


}

